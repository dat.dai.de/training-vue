import {userService} from '../services/user.service';
import {router} from '../_helpers/router';

const user = JSON.parse(localStorage.getItem('user'));
const state = user
    ? {status: {loggedIn: true}, user, msg: null}
    : {status: {}, user: null, msg: null};

const actions = {
    login({commit}, {username, password}) {
        commit('loginRequest', {username});

        userService.login(username, password)
            .then(
                user => {
                    commit('loginSuccess', user);
                    router.push('/');
                },
                error => {
                    commit('loginFailure', error);
                }
            );
    },
    logout({commit}) {
        userService.logout();
        commit('logout');
    },
    register({dispatch, commit}, user) {
        commit('registerRequest', user);

        userService.register(user)
            .then(
                user => {
                    commit('registerSuccess', user);
                    router.push('/login');
                    setTimeout(() => {
                        // display success message after route change completes
                        dispatch('alert/success', 'Registration successful', {root: true});
                    })
                },
                error => {
                    commit('registerFailure', error);
                    dispatch('alert/error', error, {root: true});
                }
            );
    },
    clearMessage({commit}) {
        commit('clearMsg');
    }
};

const mutations = {
    loginRequest(state, user) {
        state.status = {loggingIn: true};
        state.user = user;
    },
    loginSuccess(state, user) {
        state.status = {loggedIn: true};
        state.user = user;
    },
    loginFailure(state, msg) {
        state.status = {loggingIn: false};
        state.user = null;
        state.msg = msg;
    },
    logout(state) {
        state.status = {};
        state.user = null;
    },
    clearMsg(state){
        state.msg = ''
    }
};

const getters = {
    getMsg: state => {
        return state.msg;
    },

    getUser: state =>{
        return state.user;
    }
};

export const account = {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
};
