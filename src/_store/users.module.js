import { userService } from '../services/user.service';

const state = {
    all: {},
    isOk: true, message: '', user : {}
};

const actions = {
    getAll({ commit }, [page, size]) {
        commit('getAllRequest');

        userService.getAll(page, size)
            .then(
                data => commit('getAllSuccess', data),
                error => commit('getAllFailure', error)
            );
    },

    updateUser({ commit }, user){
        userService.update(user).then(
            success => commit('updateSuccess', success.message),
            error => commit('updateError', error)
        )
    },
    createUser({ commit }, user){
        userService.addNewUser(user).then(
            success => commit('updateSuccess', success.message),
            error => commit('updateError', error)
        )
    },
    clearMessage({commit}) {
        commit('clearMsg');
    },
    delete({ commit }, id) {
        commit('deleteRequest', id);

        userService.delete(id)
            .then(
                // eslint-disable-next-line no-unused-vars
                success => commit('updateSuccess', success.message),
                error => commit('updateError', error)
            );
    },
    getUser({commit}){
        userService.getByToken()
            .then(
                success => {
                    success.data.dob = new Date(success.data.dob);
                    commit('getUser', success.data)
                },
                error => commit('updateError', error)
            );
    },
    changePassword({commit}, obj){
        userService.changePassword(obj).then(
            success => commit('updateSuccess', success.message),
            error => commit('updateError', error)
        )
    }
};

const mutations = {
    getAllRequest(state) {
        state.all = { loading: true };
    },
    getAllSuccess(state, data) {
        state.all = { items: data.items, total : data.total};
    },
    getAllFailure(state, error) {
        state.all = { error };
    },

    updateSuccess(state, message){
        state.message = message;
        state.isOk = true;
    },
    updateError(state, message){
        state.message = message;
        state.isOk = false;

    },
    getUser(state, data){
        state.user = data;
    },
    clearMsg(state){
        state.message = ''
    }
    // deleteRequest(state, id) {
    //     // add 'deleting:true' property to user being deleted
    //     state.all.items = state.all.items.map(user =>
    //         user.id === id
    //             ? { ...user, deleting: true }
    //             : user
    //     )
    // },
    // deleteSuccess(id) {
    //     // remove deleted user from state
    //     state.all.items = state.all.items.filter(user => user.id !== id)
    // },
    // deleteFailure({ id, error }) {
    //     // remove 'deleting:true' property and add 'deleteError:[error]' property to user
    //     state.all.items = state.items.map(user => {
    //         if (user.id === id) {
    //             // make copy of user without 'deleting:true' property
    //             const { deleting, ...userCopy } = user;
    //             // return copy of user with 'deleteError:[error]' property
    //             return { ...userCopy, deleteError: error };
    //         }
    //
    //         return user;
    //     })
    // }
};

export const users = {
    namespaced: true,
    state,
    actions,
    mutations
};
