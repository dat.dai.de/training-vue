import Vue from 'vue';

import { store } from './_store';
import { router } from './_helpers/router';
import App from './App';
import ToastService from 'primevue/toastservice';
import Toast from 'primevue/toast';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import ColumnGroup from 'primevue/columngroup';
import InputText from 'primevue/inputtext';
import Button from 'primevue/button';
import Checkbox from 'primevue/checkbox';
import Dialog from 'primevue/dialog';
import Calendar from 'primevue/calendar';
import 'primeflex/primeflex.css';

Vue.use(ToastService);
Vue.component('DataTable', DataTable);
Vue.component('Column', Column);
Vue.component('ColumnGroup', ColumnGroup);
Vue.component('Toast', Toast);
Vue.component('InputText', InputText);
Vue.component('Button', Button);
Vue.component('Checkbox', Checkbox);
Vue.component('Dialog', Dialog);
Vue.component('Calendar', Calendar);

import 'primevue/resources/themes/nova-light/theme.css';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';

Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
