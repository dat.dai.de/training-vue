import config from '../_helpers/config';
import {authHeader} from '../_helpers/auth-header';
import axios from 'axios'

export const userService = {
    login,
    logout,
    addNewUser,
    getAll,
    getByToken,
    update,
    delete: _delete,
    changePassword
};

function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        data: {
            'username' : username,
            'password' : password
        },
        url: `${config.apiUrl}/api/login`
    };

    return axios(requestOptions)
        .then(response => {
            const user = response.data.data;
            user.token = response.headers.authorization;
            localStorage.setItem('user', JSON.stringify(user));
            return user;
        }).catch(handleError)
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

function addNewUser(user) {
    const requestOptions = {
        method: 'POST',
        headers: {...authHeader(), 'Content-Type': 'application/json'},
        data: user,
        url: `${config.apiUrl}/api/create-user`
    };

    return axios(requestOptions).then(handleResponse).catch(handleError);
}

function getAll(page,size) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `${config.apiUrl}/api/get-all-user?page=${page}&size=${size}`
    };

    return axios(requestOptions).then(response => {
        return response.data.data;
    }).catch(handleError);
}


function getByToken() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
        url: `${config.apiUrl}/api/get-user`
    };

    return axios(requestOptions).then(handleResponse).catch(handleError);
}

function update(user) {
    const requestOptions = {
        method: 'PUT',
        headers: {...authHeader(), 'Content-Type': 'application/json'},
        data: user,
        url: `${config.apiUrl}/api/update-user`
    };

    return axios(requestOptions).then(handleResponse).catch(handleError);
}

function changePassword(obj) {
    const requestOptions = {
        method: 'PUT',
        headers: {...authHeader(), 'Content-Type': 'application/json'},
        data: obj,
        url: `${config.apiUrl}/api/change-pass`
    };

    return axios(requestOptions).then(handleResponse).catch(handleError);
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader(),
        url: `${config.apiUrl}/api/delete-user/${id}`
    };

    return axios(requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.data;
}

function handleError(error) {
    let res = error.response;
    if(res.status === 401){
        logout();
        location.reload(true);
    }
    return Promise.reject(res.data.message);
}
